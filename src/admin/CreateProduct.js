import UserContext from "../UserContext";
import { useContext } from "react";
import { useParams } from "react-router-dom";
import { Navigate, useNavigate } from "react-router-dom";
import NavBar from "./components/NavBar";
import { Fragment } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";

export default function CreateProduct() {
   

  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  let access = JSON.parse(user.isAdmin);
  const params = useParams();
  const [name, setName] = useState("");
  let [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (name !== "" && price !== "" && description !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    console.log(name)
    console.log(price)
    console.log(description)
  });

  function clearFields() {
    setName("");
    setPrice("");
    setDescription("");
  }

  const createProduct = async (e) => {
    e.preventDefault();
    console.log(user);
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
          body: JSON.stringify({
            name: name,
            price: price,
            description: description,
          }),
        }
      )
        .then((res) => res.json())
        .then((data) => {
     
          if (data.message == "success") {
            Swal.fire({
              title: "Create Successful",
              icon: "success",
              text: "Product has been added successfully!",
            });
            clearFields();
            navigate("/admin/products");
          } else if(data.message == 'exists'){
            Swal.fire({
              title: "Creation Fail",
              icon: "error",
              text: "Product is already exists",
            });
            clearFields()
          }else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please try again later",
              });
              clearFields()
          }

        console.log(data)
        });
    } catch (error) {
      console.log(error);
    }
  };

 

  
  return access != true ? (
    <Navigate to="/restricted" />
  ) : (
    <Fragment>
      <NavBar />
      <Container>
        <Row>
          <Col></Col>
          <Col sm>
            <Card className="mb-2 bg-dark text-light mt-5">
              <Card.Header>
                <h1>Create Product</h1>
              </Card.Header>
              <Card.Body>
                <Form onSubmit={(e) => createProduct(e)}>
                  <Form.Group controlId="userEmail" className="mt-3">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Product Name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="password" className="mt-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="number"
                      placeholder="Price"
                      value={price}
                      onChange={(e) => setPrice(e.target.value)}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="password" className="mt-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                      style={{ height: "100px" }}
                      required
                    />
                  </Form.Group>
                  {isActive ? (
                    <Button
                      variant="warning"
                      type="submit"
                      id="submitBtn"
                      className="mt-3"
                    >
                      Create
                    </Button>
                  ) : (
                    <Button
                      variant="warning"
                      type="submit"
                      id="submitBtn"
                      disabled
                      className="mt-3"
                    >
                      Create
                    </Button>
                  )}
                  <br /> <br />
                </Form>
              </Card.Body>
            </Card>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </Fragment>
  );
}