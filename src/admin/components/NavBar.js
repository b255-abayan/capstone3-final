import { Link } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import UserContext from "../../UserContext";

export default function NavBar() {
  const { user, setUser } = useContext(UserContext);
  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark p-0 m-0">
        <div className="container-fluid">
          {/* <a className="navbar-brand" href="#">
            <span>E-COMMERCE</span>
          </a> */}
          <Link to="/admin/products" className="dropdown-item navbar-brand">
            E-COMMERCE
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className=" collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav ms-auto ">
              {/* <li className="nav-item">
                <a
                  className="nav-link mx-2 active"
                  aria-current="page"
                  href="/"
                >
                  Store
                </a>
              </li> */}

              <li className="nav-item dropdown">
                <a
                  className="nav-link mx-2 dropdown-toggle"
                  href="#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Products
                </a>
                <ul
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <li>
                    <Link to="/admin/products" className="dropdown-item">
                      Product List
                    </Link>
                  </li>
                  <li>
                    <Link to="/admin/products/create" className="dropdown-item">
                      Create Product
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <a
                  className="nav-link mx-2 dropdown-toggle"
                  href="#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Users
                </a>
                <ul
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <li>
                    <Link to="/admin/users" className="dropdown-item">
                      User List
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link mx-2 active"
                  aria-current="page"
                  href="/admin/orders/history"
                >
                  Orders
                </a>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link mx-2 dropdown-toggle"
                  href="#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Hello, {user.firstName}
                </a>
                <ul
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <li>
                    <Link to="/logout" className="dropdown-item">
                      Logout
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}
