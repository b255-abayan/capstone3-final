import React, { useEffect, useState } from "react";
import UserContext from "../UserContext";
import { useContext } from "react";
import NavBar from "./components/NavBar";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
export default function Users() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);

  const fetchData = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/users`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setUsers(data);
      });
  };

  const makeAdminUser = async (userId) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/${userId}/grant-admin`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          // if (data.message === "success") {
          //   Swal.fire({
          //     title: "Archieved Successful",
          //     icon: "success",
          //     text: "Product has been disabled successfully!",
          //   });

          // }
          fetchData();
        });
    } catch (error) {
      console.log(error);
    }
  };

  const grantAdmin = async (userId) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/grant-admin`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
          body: JSON.stringify({
            userId: userId,
          }),
        }
      )
        .then((res) => res.json())
        .then((data) => {
          Swal.fire({
            title: "Update Successful",
            icon: "success",
            text: "User has now granted for admin access!",
          });
          fetchData();
        });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Fragment>
      <NavBar />
      <div className="container mt-3">
        <p className="text-right">
          <button className="btn btn-sm btn-success text-right">
            Create <i className="bi bi-plus-circle"></i>
          </button>
        </p>
        <h1>User List</h1>
        <strong>
          <table className="table table-responsive table-striped table-hover">
            <thead className="thead-dark">
              <tr>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">Admin</th>
                <th scope="col">Action</th>
              </tr>
            </thead>

            {users.length > 0 && (
              <tbody id="products">
                {users.map((user) => (
                  <tr key={user._id}>
                    <td>{user.firstName}</td>
                    <td>{user.lastName}</td>
                    <td>{user.email}</td>
                    {user.isAdmin ? (
                      <td>
                        <p className="text-success">yes</p>
                      </td>
                    ) : (
                      <td>
                        <p className="text-danger">no</p>
                      </td>
                    )}

                    {user.isAdmin ? (
                      <td>
                        <i
                          className="bi bi-dash-square-fill text-success"
                          onClick={() => {
                            makeAdminUser(user._id);
                          }}
                        ></i>
                        <Link to={`/admin/products/update/${user._id}`}></Link>
                      </td>
                    ) : (
                      <td>
                        <i
                          className="bi bi-dash-square-fill text-danger"
                          onClick={() => {
                            grantAdmin(user._id);
                          }}
                        ></i>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            )}
          </table>
        </strong>
      </div>
    </Fragment>
  );
}
