import UserContext from "../UserContext";
import { useContext } from "react";
import { useParams } from "react-router-dom";
import { Navigate, useNavigate } from "react-router-dom";
import NavBar from "./components/NavBar";
import { Fragment } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";

export default function ProductUpdate() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  let access = JSON.parse(user.isAdmin);
  const params = useParams();
  const [name, setName] = useState("");
  let [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [newName, setNewName] = useState("");

  useEffect(() => {
    if (name !== "" && price !== "" && description !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }

    const fetchData = async () => {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/${params.productId}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          // setName(data.name);
          // setPrice(data.price)
          // setDescription(data.description);
          let productName = document.querySelector("#product").value;
          productName = data.name;
        });
    };

    fetchData();
  });

  function clearFields() {
    setName("");
    setPrice("");
    setDescription("");
  }

  const updateProduct = async (e) => {
    e.preventDefault();
    // console.log(user);
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/${params.productId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
          body: JSON.stringify({
            name: name,
            price: parseInt(price),
            description: description,
            isActive: true,
          }),
        }
      )
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if (data.message == "success") {
            Swal.fire({
              title: "Update Successful",
              icon: "success",
              text: "Product has been updated successfully!",
            });
            clearFields();
            navigate("/admin/products");
          } else {
            Swal.fire({
              title: "Something wen't wrong",
              icon: "error",
              text: "Please try again later.",
            });
          }
        });
    } catch (error) {
      console.log(error);
    }
  };
  return access != true ? (
    <Navigate to="/restricted" />
  ) : (
    <Fragment>
      <NavBar />
      <Container>
        <Row>
          <Col></Col>
          <Col sm>
            <Card className="mb-2 bg-dark text-light mt-5">
              <Card.Header>
                <h1>Edit Product</h1>
              </Card.Header>
              <Card.Body>
                <Form onSubmit={(e) => updateProduct(e)}>
                  <Form.Group controlId="product" className="mt-3">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Product Name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="price" className="mt-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="number"
                      placeholder="Price"
                      value={price}
                      onChange={(e) => setPrice(e.target.value)}
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="description" className="mt-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Description"
                      value={description}
                      onChange={(e) => setDescription(e.target.value)}
                      style={{ height: "100px" }}
                      required
                    />
                  </Form.Group>
                  {isActive ? (
                    <Button
                      variant="warning"
                      type="submit"
                      id="submitBtn"
                      className="mt-3"
                    >
                      Update
                    </Button>
                  ) : (
                    <Button
                      variant="warning"
                      type="submit"
                      id="submitBtn"
                      disabled
                      className="mt-3"
                    >
                      Update
                    </Button>
                  )}
                  <br /> <br />
                </Form>
              </Card.Body>
            </Card>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </Fragment>
  );
}

// import UserContext from "../UserContext";
// import { useContext } from "react";
// import { Navigate, useNavigate } from "react-router-dom";
// export default function Product() {
//   const navigate = useNavigate();
//   const { user } = useContext(UserContext);
//   let access = JSON.parse(user.isAdmin);
//   console.log(access);
//   return access != true ? <Navigate to="/restricted" /> : <h1>Hello</h1>;

// }
