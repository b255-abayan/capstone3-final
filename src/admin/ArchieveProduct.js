import UserContext from "../UserContext";
import { useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
export default function ArchieveProduct() {
  const params = useParams();
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  let access = JSON.parse(user.isAdmin);

  const archieveProduct = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/${params.productId}/archieve`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          if (data.message === "success") {
            Swal.fire({
              title: "Update Successful",
              icon: "success",
              text: "Product has been updated successfully!",
            });
            navigate("/admin/products");
          }
        });
    } catch (error) {
      console.log(error);
    }
  };
  archieveProduct();

  return access != true ? (
    <Navigate to="/restricted" />
  ) : (
    <h1>Hello, {params.productId}</h1>
  );
}
