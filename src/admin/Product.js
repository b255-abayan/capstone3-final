import React, { useEffect, useState } from "react";
import UserContext from "../UserContext";
import { useContext } from "react";
import NavBar from "./components/NavBar";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
export default function Product() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [products, setProducts] = useState([]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  // Archieve Product
  const archieveProduct = async (productId) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/${productId}/archieve`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          if (data.message === "success") {
            Swal.fire({
              title: "Archieved Successful",
              icon: "success",
              text: "Product has been disabled successfully!",
            });
          }
          fetchData();
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Activate Product
  const activateProduct = async (productId) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/products/${productId}/activate`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${user.token}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          if (data.message === "success") {
            Swal.fire({
              title: "Activate Successful",
              icon: "success",
              text: "Product has been activated successfully!",
            });
          }
          fetchData();
        });
    } catch (error) {
      console.log(error);
    }
    console.log("it works");
  };

  function thousands_separators(num) {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
  }

  return (
    <Fragment>
      <NavBar />
      <div className="container mt-3">
        <p className="text-right">
          {/* <button className="btn btn-sm btn-success text-right">
      Create <i className="bi bi-plus-circle"></i>
    </button> */}
          <Link
            to="/admin/products/create"
            className="btn btn-sm btn-success text-right"
          >
            Create <i className="bi bi-plus-circle"></i>
          </Link>
        </p>
        <h1>Product List</h1>
        <strong>
          <table className="table table-responsive table-striped table-hover">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Price</th>
                <th scope="col">Active</th>
                <th scope="col">Action</th>
              </tr>
            </thead>

            {products.length > 0 && (
              <tbody id="products">
                {products.map((product) => (
                  <tr key={product._id}>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>
                      <span>{"₱ "}</span>
                      {product.price}
                    </td>
                    {product.isActive ? (
                      <td>
                        <p className="text-success">yes</p>
                      </td>
                    ) : (
                      <td>
                        <p className="text-danger">no</p>
                      </td>
                    )}

                    {product.isActive ? (
                      <td>
                        <i
                          className="bi bi-eye text-success"
                          onClick={() => {
                            archieveProduct(product._id);
                          }}
                        ></i>{" "}
                        |{" "}
                        <Link to={`/admin/products/update/${product._id}`}>
                          <i className="bi bi-pencil-square text-warning"></i>
                        </Link>
                      </td>
                    ) : (
                      <td>
                        <i
                          className="bi bi-eye-slash text-danger"
                          onClick={() => {
                            activateProduct(product._id);
                          }}
                        ></i>{" "}
                        | <i className="bi bi-pencil-square text-secondary"></i>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            )}
          </table>
        </strong>
      </div>
    </Fragment>
  );
}
