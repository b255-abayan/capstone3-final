import { Fragment } from "react";
import { Link } from "react-router-dom";
export default function NotFound() {
  return (
    <Fragment>
      {/* <div className="inner_page error_404">
        <div className="full_container">
          <div className="container">
            <div className="center verticle_center full_height">
              <div className="error_page">
                <div className="center">
                  <div className="error_icon">
                    <img
                      className="img-responsive"
                      src="./backend/images/layout_img/error.png"
                      alt="#"
                    />
                  </div>
                </div>
                <br />
                <h3>PAGE NOT FOUND !</h3>
                <p>YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
                <div className="center">
                  <Link to="/" exact="true" className="main_bt">
                    Go To Home Page
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div> */}

      <div className="login-page">
        <div>
          <div className="error_page">
            <div className="center">
              <div className="error_icon">
                <img className="img-responsive" src="./error.png" alt="#" />
              </div>
            </div>
            <br />
            <h3 className="text-center">PAGE NOT FOUND !</h3>
            <p className="text-center">
              You seem to be trying to access a page that doesn't exists.
            </p>
            <div className="text-center">
              <Link to="/" exact="true" className="btn btn-success">
                Go To Home Page
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
