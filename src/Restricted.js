import { Fragment } from "react";
import { Link } from "react-router-dom";
export default function Restricted() {
  return (
    <Fragment>
      

      <div className="login-page">
        <div>
          <div className="error_page">
            <div className="center">
              <div className="error_icon">
                <img className="img-responsive" src="./error.png" alt="#" />
              </div>
            </div>
            <br />
            <h3 className="text-center">ACCESS DENIED !</h3>
            <p className="text-center">
            You dont have a permission to view this site.
            </p>
            <div className="text-center">
              <Link to="/" exact="true" className="btn btn-success">
                Go To Home Page
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
