import UserContext from "../UserContext";
import { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import NavBar from "./components/NavBar";
import { Link } from "react-router-dom";
export default function OrderHistory() {
  const { user } = useContext(UserContext);
  let access = JSON.parse(user.isAdmin);
  const [orders, setOrders] = useState([]);

  const fetchOrderHistory = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/orders/mycart`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrders(data);
      });
  };

  useEffect(() => {
    fetchOrderHistory();
  }, []);

  function thousands_separators(num) {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
  }

  return user.id !== null ? (
    <div>
      <NavBar />
      <div className="profile">
        <div className="mycart">
          <table className="table table-responsive table-striped table-hover">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Product</th>

                <th scope="col">Total Amount</th>
                <th scope="col">Quantity</th>
              </tr>
            </thead>

            {orders.length > 0 && (
              <tbody id="products">
                {orders.map((order) => (
                  <tr key={order._id}>
                    <td>{order.products[0].productName}</td>
                    <td>
                      <span>₱</span>
                      {thousands_separators(order.totalAmount)}
                    </td>
                    <td>{order.products[0].quantity}</td>
                  </tr>
                ))}
              </tbody>
            )}
          </table>
        </div>
      </div>
    </div>
  ) : (
    <Navigate to="/" />
  );
}
