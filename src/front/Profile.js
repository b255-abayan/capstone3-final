import UserContext from "../UserContext";
import { useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import NavBar from "./components/NavBar";
export default function Profile() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  let access = JSON.parse(user.isAdmin);

  return user.id !== null ? (
    <div>
      <NavBar />
      <div className="profile">
        <div className="profile-content">
          <div className="">
            <div className="accordion" id="accordionExample">
              <div className="accordion-item">
                <h2 className="accordion-header" id="headingOne">
                  <button
                    className="accordion-button"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseOne"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    <h2>User Account</h2>
                  </button>
                </h2>
                <div
                // id="collapseOne"
                // className="accordion-collapse collapse show"
                // aria-labelledby="headingOne"
                // data-bs-parent="#accordionExample"
                >
                  <div className="accordion-body">
                    <h3 className="text-center">
                      {user.firstName} {user.lastName}
                    </h3>
                    <h5 className="text-center">{user.email}</h5>
                  </div>
                </div>
              </div>
            </div>

            <div className="accordion" id="accordionExample">
              <div className="accordion-item">
                <h2 className="accordion-header" id="headingOne">
                  <button
                    className="accordion-button"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseOne"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    <h2>Change Password</h2>
                  </button>
                </h2>
                <div
                // id="collapseOne"
                // className="accordion-collapse collapse show"
                // aria-labelledby="headingOne"
                // data-bs-parent="#accordionExample"
                >
                  <div className="accordion-body">
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Curent Password"
                      ></input>
                    </div>
                  </div>
                  <div className="accordion-body">
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="New Password"
                      ></input>
                    </div>
                  </div>
                  <div className="accordion-body">
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Verify Password"
                      ></input>
                    </div>
                  </div>
                  <button type="submit" className="btn btn-danger m-3">
                    Change Password
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <Navigate to="/" />
  );
}
