import { Link } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import UserContext from "../../UserContext";
import { Fragment } from "react";
import React from "react";
export default function NavBar() {
  const { user, setUser } = useContext(UserContext);
  const [cart, setCart] = useState(0);
  let access = "";
  let navAccess = null;
  if (localStorage.isAdmin != null) {
    access = JSON.parse(localStorage.isAdmin);
  }

  const fetchCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/mycart`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        // console.log(data);
        if (data.auth == "Authentication Failed") {
          setCart(0);
        } else if (data.length <= 0) {
          setCart(0);
        } else {
          setCart(data[0].myCartCount);
        }
      });
  };
  
  // useEffect(() => {
  //   if (cart > 0) {
  //     console.log(cart);
  //   }
  // }, cart);
  useEffect(() => {
    fetchCart();
  }, []);

  const getIp = () => {
    fetch(`https://api.bigdatacloud.net/data/client-ip`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {


        fetch(`${process.env.REACT_APP_API_URL}/ip`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            ipAddress: data.ipString,
          }),
        });
      });
  };
  return (
    <header className="header_section">
      <div className="header_bottom">
        <div className="container-fluid">
          <nav className="navbar navbar-expand-lg custom_nav-container ">
            <Link to="/" exact="true" className="navbar-brand">
              {/* <span>Ecommerce</span> */}
              <img
                src="/web-logo.png"
                width="60"
                height="60"
                className="d-inline-block align-top"
                alt="React Bootstrap logo"
              />
            </Link>

            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className=""> </span>
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav ">
                {/* <!-- nav-link --> */}
                {user.id != null && access === true ? (
                  <React.Fragment>
                    <li className="nav-item active">
                      <Link to="/profile" exact="true" className="nav-link">
                        <i className="fa fa-user" aria-hidden="true"></i>
                      </Link>
                    </li>

                    <li className="nav-item active">
                      <Link
                        to="/admin/products"
                        exact="true"
                        className="nav-link"
                      >
                        Admin
                      </Link>
                    </li>

                    <li className="nav-item active">
                      <Link to="/logout" exact="true" className="nav-link">
                        Logout
                      </Link>
                    </li>
                  </React.Fragment>
                ) : user.id != null && access === false ? (
                  <React.Fragment>
                    <li className="nav-item active">
                      <Link to="/profile" exact="true" className="nav-link">
                        <i className="fa fa-user" aria-hidden="true"></i>
                      </Link>
                    </li>

                    <li className="nav-item active">
                      <Link to="/mycart" exact="true" className="nav-link">
                        <i className="fa fa-shopping-cart" aria-hidden="true">
                          <span
                            className="badge badge-light m-1"
                            style={{ backgroundColor: "red" }}
                          >
                            {cart}
                          </span>
                        </i>
                      </Link>
                    </li>
                    <li className="nav-item active">
                      <Link
                        to="/orders/history"
                        exact="true"
                        className="nav-link"
                      >
                        Order History
                      </Link>
                    </li>
                    <li className="nav-item active">
                      <Link to="/logout" exact="true" className="nav-link">
                        Logout
                      </Link>
                    </li>
                  </React.Fragment>
                ) : access === "" ? (
                  <Fragment>
                    <li className="nav-item active">
                      <Link to="/login" exact="true" className="nav-link">
                        Login
                      </Link>
                    </li>
                    <li className="nav-item active">
                      <Link to="/register" exact="true" className="nav-link">
                        Register
                      </Link>
                    </li>
                  </Fragment>
                ) : (
                  false
                )}

                {/* {user.id !== null ? (
                  user.isAdmin == "true" ? (
                    <React.Fragment>
                      <li className="nav-item active">
                        <Link to="/account" exact="true" className="nav-link">
                          <i className="fa fa-user" aria-hidden="true"></i>
                        </Link>
                      </li>

                      <li className="nav-item active">
                        <Link
                          to="/admin/products"
                          exact="true"
                          className="nav-link"
                        >
                          Admin
                        </Link>
                      </li>

                      <li className="nav-item active">
                        <Link to="/logout" exact="true" className="nav-link">
                          Logout
                        </Link>
                      </li>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <li className="nav-item active">
                        <Link to="/account" exact="true" className="nav-link">
                          <i className="fa fa-user" aria-hidden="true"></i>
                        </Link>
                      </li>

                      <li className="nav-item active">
                        <Link to="/mycart" exact="true" className="nav-link">
                          <i
                            className="fa fa-shopping-cart"
                            aria-hidden="true"
                          ></i>
                        </Link>
                      </li>
                      <li className="nav-item active">
                        <Link to="/logout" exact="true" className="nav-link">
                          Logout
                        </Link>
                      </li>
                    </React.Fragment>
                  )
                ) : (
                  <Fragment>
                    <li className="nav-item active">
                      <Link to="/login" exact="true" className="nav-link">
                        Login
                      </Link>
                    </li>
                    <li className="nav-item active">
                      <Link to="/register" exact="true" className="nav-link">
                        Register
                      </Link>
                    </li>
                  </Fragment>
                )} */}

                {/* <!-- end nav-link --> */}
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </header>
  );
}
