import UserContext from "../UserContext";
import { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import NavBar from "./components/NavBar";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
export default function MyCart() {
  const { user } = useContext(UserContext);
  let access = JSON.parse(user.isAdmin);
  const [carts, setCarts] = useState([]);
  const fetchCart = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/carts/mycart-items`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setCarts(data);
      });
  };

  const removeCartItem = async (cartId) => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/carts/${cartId}/delete`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        fetchCart();
      });
  };

  const checkOut = () => {
    carts.forEach((cart) => {
      const response = fetch(`${process.env.REACT_APP_API_URL}/orders`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          userId: cart.userId,
          productId: cart.products[0].productId,
          quantity: cart.products[0].quantity,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          Swal.fire({
            title: "Order Successful",
            icon: "success",
            text: "You successfully placed an order!",
          });
          removeCartItem(cart._id);
          fetchCart();
        });
    });
  };

  useEffect(() => {
    fetchCart();
  }, []);

  return user.id !== null ? (
    <div>
      <NavBar />
      <div className="profile">
        <div className="mycart">
          <table className="table table-responsive table-striped table-hover">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Product</th>

                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Action</th>
              </tr>
            </thead>

            {carts.length > 0 && (
              <tbody id="products">
                {carts.map((cart) => (
                  <tr key={cart._id}>
                    <td>{cart.products[0].productName}</td>

                    <td>
                      <span>₱</span>
                      {cart.itemPrice}
                    </td>
                    <td>{cart.products[0].quantity}</td>
                    <td>
                      <span>₱</span>
                      {cart.totalAmount}
                    </td>
                    <td>
                      <span
                        className="btn btn-danger"
                        onClick={() => {
                          removeCartItem(cart._id);
                        }}
                      >
                        Remove
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            )}
          </table>
          <span
            className="btn btn-success"
            style={{
              width: "100%",
            }}
            onClick={() => {
              checkOut();
            }}
          >
            <strong>Proceed Checkout</strong>
          </span>
        </div>
      </div>
    </div>
  ) : (
    <Navigate to="/" />
  );
}
