import { Link } from "react-router-dom";
import UserContext from "../UserContext";
import { useState, useEffect, useContext } from "react";
import Swal from "sweetalert2";
import { Navigate, useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";
export default function Register() {
  const navigate = useNavigate();
  // const { user, setUser } = useContext(UserContext);
  const { user } = useContext(UserContext);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setisActive] = useState(false);

  const registerUser = async (e) => {
    e.preventDefault();
    //check if the email is already exist
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/users/checkEmail`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please provide a different email",
          });
        } else {
          // If no email duplicate proceed with the registration
          register();
        }
      });

    setFirstName("");
    setLastName("");
    setEmail("");
    setPassword1("");
    setPassword2("");
  };

  const register = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/users/register`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password2,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        // If post request is successfully executed with true response, fire the success alert and redirect the page to "login"
        if (data.message === "User created successfully") {
          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Ecommerce!",
          });
          // Redirect to login page upon successful registration
          navigate("/login");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again later",
          });
        }
      });
  };
  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setisActive(true);
    } else {
      setisActive(false);
    }
  }, [firstName, lastName, email, password1, password2]);
  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <div className="form-body">
      <div className="login-page">
        <div className="form">
          <h1>Sign Up</h1>
          <form className="login-form" onSubmit={(e) => registerUser(e)}>
            <input
              type="text"
              placeholder="First Name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
            <input
              type="text"
              placeholder="Last Name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
            <input
              type="email"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
            />
            <input
              type="password"
              placeholder="Verify Password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
            />
            {isActive ? (
              <Button
                variant="warning"
                type="submit"
                id="submitBtn"
                className="mt-3"
              >
                Sign In
              </Button>
            ) : (
              <Button
                variant="warning"
                type="submit"
                id="submitBtn"
                disabled
                className="mt-3"
              >
                Sign In
              </Button>
            )}

            <p className="message">
              Already have an account?
              <Link to="/login" exact="true">
                {" Sign In"}
              </Link>
              <Link to="/" exact="true" className="nav-link text-info">
                Home
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}
