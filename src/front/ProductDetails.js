import { useEffect, useState } from "react";
import NavBar from "./components/NavBar";
import { Link, useParams } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext } from "react";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";
export default function ProductDetails() {
  const { user } = useContext(UserContext);
  const [product, setProduct] = useState([]);
  const params = useParams();
  const [minusBtn, setMinusBtn] = useState(true);
  const navigate = useNavigate();
  let [quantity, setQuantity] = useState(1);
  const [addToCartBtn, setAddToCartBtn] = useState(true);
  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${params.productId}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setProduct(data);
      });
  };

  function thousands_separators(num) {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
  }

  const addToCart = async (e) => {
    e.preventDefault();
    if (user.id == null) {
      navigate("/login");
    }
    const response = await fetch(`${process.env.REACT_APP_API_URL}/carts`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify({
        productId: params.productId,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message == "success") {
          Swal.fire({
            title: "Add Successful",
            icon: "success",
            text: "Add to cart successfully!",
          });
          fetchData();
        }
      });
  };

  const minus = () => {
    setQuantity((quantity = quantity - 1));
  };
  const plus = () => {
    setQuantity((quantity = quantity + 1));
  };

  useEffect(() => {
    fetchData();
    if (quantity <= 0) {
      setMinusBtn(false);
      setAddToCartBtn(false);
    } else {
      setMinusBtn(true);
      setAddToCartBtn(true);
    }
  }, [quantity]);
  return (
    <div>
      <NavBar />
      <div className="container mt-5 mb-5">
        <div className="row d-flex justify-content-center">
          <div className="col-md-10">
            <div className="card">
              <div className="row">
                <div className="col-md-6">
                  <div className="images p-3">
                    <div className="text-center p-4">
                      {" "}
                      <img
                        id="main-image"
                        src="/frontend/images/p1.png"
                        width="250"
                      />{" "}
                    </div>
                    <div className="thumbnail text-center">
                      {" "}
                      <img
                        /*onClick="change_image(this)"*/ src="/frontend/images/p1.png"
                        width="70"
                      />{" "}
                      <img
                        /*onClick="change_image(this)"*/ src="/frontend/images/p1.png"
                        width="70"
                      />{" "}
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  {/* Form */}
                  <form onSubmit={(e) => addToCart(e)}>
                    <div className="product p-4">
                      <div className="d-flex justify-content-between align-items-center">
                        <div className="d-flex align-items-center">
                          {" "}
                          <span className="ml-1">
                            <Link to={"/"}>
                              <i className="fa fa-long-arrow-left"></i> Back
                            </Link>
                          </span>{" "}
                        </div>{" "}
                        <i className="fa fa-shopping-cart text-muted"></i>
                      </div>
                      <div className="mt-4 mb-3">
                        {" "}
                        <span className="text-uppercase text-muted brand"></span>
                        <h5 className="text-uppercase">{product.name}</h5>
                        <div className="price d-flex flex-row align-items-center">
                          {" "}
                          <span className="act-price">₱ {product.price}</span>
                          {/* <div className="ml-2"> <small className="dis-price">$59</small> <span>40% OFF</span> </div> */}
                        </div>
                      </div>
                      <p className="about">{product.description}</p>
                      {/* <div className="sizes mt-5">
                                            <h6 className="text-uppercase">Size</h6> <label className="radio"> <input type="radio" name="size" value="S"  /> <span>S</span> </label> <label className="radio"> <input type="radio" name="size" value="M"/> <span>M</span> </label> <label className="radio"> <input type="radio" name="size" value="L"/> <span>L</span> </label> <label className="radio"> <input type="radio" name="size" value="XL"/> <span>XL</span> </label> <label className="radio"> <input type="radio" name="size" value="XXL"/> <span>XXL</span> </label>
                                        </div> */}

                      {/* quantity*/}

                      <div className="input-group"></div>
                      {minusBtn ? (
                        <span
                          className="btn btn-secondary"
                          style={{ borderRadius: 0 }}
                          onClick={() => {
                            minus();
                          }}
                        >
                          -
                        </span>
                      ) : (
                        <span
                          className="btn btn-secondary"
                          style={{ borderRadius: 0 }}
                          onClick={() => {
                            minus();
                          }}
                          disabled
                        >
                          -
                        </span>
                      )}

                      <input
                        type="number"
                        style={{ width: "60px", height: "35px" }}
                        className=""
                        value={quantity}
                        onChange={(e) => setQuantity(e.target.value)}
                      ></input>
                      <span
                        className="btn btn-secondary"
                        style={{ borderRadius: 0 }}
                        onClick={() => {
                          plus();
                        }}
                      >
                        +
                      </span>
                      {/* quantity*/}
                      {user.id != null && JSON.parse(user.isAdmin) ? (
                        ""
                      ) : (
                        <div className="cart mt-4 align-items-center">
                          {" "}
                          {addToCartBtn ? (
                            <button
                              className="btn btn-danger text-uppercase mr-2 px-4"
                              type="submit"
                            >
                              Add to cart
                            </button>
                          ) : (
                            <button
                              className="btn btn-danger text-uppercase mr-2 px-4"
                              disabled
                            >
                              Add to cart
                            </button>
                          )}
                        </div>
                      )}
                    </div>
                  </form>
                  {/* Form */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
