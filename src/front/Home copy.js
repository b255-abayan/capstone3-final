import NavBar from "./components/NavBar";
export default function Home() {
  return (
    <div>
      <NavBar />
      <section className="product_section layout_padding">
        <div className="container">
          <div className="heading_container heading_center">
            <h2>Our Products</h2>
          </div>
          <div className="row">
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p1.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p2.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p3.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p4.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p5.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p6.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p7.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p8.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p9.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <footer className="footer_section">
        <div className="container">
          <p>
            &copy; <span id="displayYear"></span> All Rights Reserved
          </p>
        </div>
      </footer>
    </div>
  );
}
