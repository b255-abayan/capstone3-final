import NavBar from "./components/NavBar";
import React, { useEffect, useState } from "react";
import UserContext from "../UserContext";
import { useContext } from "react";
import { Link } from "react-router-dom";
export default function Home() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);

  const fetchData = async() => {
    await fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
    getIp();
  }, []);

  

  const getIp = () => {
    fetch(`https://api.bigdatacloud.net/data/client-ip`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {


        fetch(`${process.env.REACT_APP_API_URL}/ip`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            ipAddress: data.ipString,
          }),
        });
      });
  };


  function thousands_separators(num) {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
  }

  return (
    <div>
      <NavBar />
      <section className="product_section layout_padding">
        <div className="container">
          <div className="heading_container heading_center">
            <h2>Our Products</h2>
          </div>

          {/* <div className="row">
            
            <div className="col-sm-6 col-lg-4">
              <div className="box">
                <div className="img-box">
                  <img src="./frontend/images/p1.png" alt="" />
                  <a href="" className="add_cart_btn">
                    <span>Add To Cart</span>
                  </a>
                </div>
                <div className="detail-box">
                  <h5>Product Name</h5>
                  <div className="product_info">
                    <h5>
                      <span>$</span> 300
                    </h5>
                    <div className="star_container">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> */}

          {products.length > 0 && (
            <div className="row">
              {products.map((product) => (
                <div className="col-sm-6 col-lg-4" key={product._id}>
                  <div className="box">
                    <div className="img-box">
                      <img src="./frontend/images/p1.png" alt="" />
                      {/* <a href="" className="add_cart_btn">
                  <span>Add To Cart</span>
                </a> */}
                      <Link
                        to={`/products/${product._id}`}
                        className="add_cart_btn"
                      >
                        View Details
                      </Link>
                      {/* {console.log(user.isAdmin)} */}
                    </div>
                    <div className="detail-box">
                      <h5>{product.name}</h5>
                      <div className="product_info" key={product.id}>
                        <h5>
                          <span>₱</span> {thousands_separators(product.price)}
                        </h5>
                        <div className="star_container">
                          <i className="fa fa-star" aria-hidden="true"></i>
                          <i className="fa fa-star" aria-hidden="true"></i>
                          <i className="fa fa-star" aria-hidden="true"></i>
                          <i className="fa fa-star" aria-hidden="true"></i>
                          <i className="fa fa-star" aria-hidden="true"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}

          {products.message === "No active product(s) found on our record" ? (
            <h2 className="text-center">No Product(s) available</h2>
          ) : (
            ""
          )}
        </div>
      </section>
      <section className="why_us_section layout_padding">
        <div className="container">
          <div className="heading_container heading_center">
            <h2>Why Choose Us</h2>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="box ">
                <div className="img-box">
                  <img src="./frontend/images/w1.png" alt="" />
                </div>
                <div className="detail-box">
                  <h5>Fast Delivery</h5>
                  <p>variations of passages of Lorem Ipsum available</p>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="box ">
                <div className="img-box">
                  <img src="./frontend/images/w2.png" alt="" />
                </div>
                <div className="detail-box">
                  <h5>Free Shiping</h5>
                  <p>variations of passages of Lorem Ipsum available</p>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="box ">
                <div className="img-box">
                  <img src="./frontend/images/w3.png" alt="" />
                </div>
                <div className="detail-box">
                  <h5>Best Quality</h5>
                  <p>variations of passages of Lorem Ipsum available</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <footer className="footer_section">
        <div className="container">
          <p>
            &copy; <span id="displayYear"></span> All Rights Reserved
          </p>
        </div>
      </footer>
    </div>
  );
}
