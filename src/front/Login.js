import { Link } from "react-router-dom";
import { useState, useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { Button } from "react-bootstrap";

export default function Login() {
  //Allows us to consume the User context object and it's properties to use for user validation
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);
  const authenticate = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/login`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: email,
            password: password,
          }),
        }
      )
        .then((res) => res.json())
        .then((data) => {
          // console.log(data);
          if (data.auth !== "Authentication Failed") {
            // localStorage.setItem("token", data.access);
            retrieveUserDetails(data.access);
            Swal.fire({
              title: "Login Successful",
              icon: "success",
              text: "Welcome to Ecommerce!",
            });
          } else {
            Swal.fire({
              title: "Authentication Failed",
              icon: "error",
              text: "Check your login details and try again.",
            });
          }
        });
      // Clear input fields after submission
      setEmail("");
      setPassword("");
    } catch (error) {
      console.log(error);
    }
  };

  const retrieveUserDetails = async (token) => {
    // The token will be sent as part of the requests header information
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_URL}/users/loggedin`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      )
        .then((res) => res.json())
        .then((data) => {
          localStorage.setItem("id", data[0]._id);
          localStorage.setItem("isAdmin", data[0].isAdmin);
          localStorage.setItem("firstName", data[0].firstName);
          localStorage.setItem("lastName", data[0].lastName);
          localStorage.setItem("token", token);
          localStorage.setItem("email", data[0].email);
          setUser({
            id: data[0]._id,
            isAdmin: data[0].isAdmin,
            firstName: data[0].firstName,
            lastName: data[0].lastName,
            token: token,
            email: email,
          });
        });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    // console.log(localStorage);
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);
  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <div className="form-body">
      <div className="login-page">
        <div className="form">
          <h1>Sign In</h1>
          <form className="login-form" onSubmit={(e) => authenticate(e)}>
            <input
              type="email"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            {/* <button>login</button> */}
            {isActive ? (
              <Button
                variant="warning"
                type="submit"
                id="submitBtn"
                className="mt-3"
              >
                Sign In
              </Button>
            ) : (
              <Button
                variant="warning"
                type="submit"
                id="submitBtn"
                disabled
                className="mt-3"
              >
                Sign In
              </Button>
            )}

            <p className="message">
              Not registered?
              <Link to="/register" exact="true">
                {" Create an account"}
              </Link>
              <Link to="/" exact="true" className="nav-link text-info">
                Home
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}
