import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import Home from "./front/Home";
import Login from "./front/Login";
import Register from "./front/Register";
import Logout from "./front/Logout";
import { UserProvider } from "./UserContext";
import { useState, useEffect } from "react";
import Product from "./admin/Product";
import NotFound from "./NotFound";
import Users from "./admin/Users";
import UpdateProduct from "./admin/UpdateProduct";
import ProductDetails from "./front/ProductDetails";
// import Test from "./admin/Test";
import CreateProduct from "./admin/CreateProduct";
import Profile from "./front/Profile";
import MyCart from "./front/MyCart";
import OrderHistory from "./front/OrderHistory";
import Restricted from "./Restricted";
import AdminOrderHistory from "./admin/AdminOrderHistory";

function App() {
  // State Hook for the user state thats deficed here for a global scope
  const [user, setUser] = useState({
    id: localStorage.getItem("id"),
    isAdmin: localStorage.getItem("isAdmin"),
    firstName: localStorage.getItem("firstName"),
    lastName: localStorage.getItem("lastName"),
    token: localStorage.getItem("token"),
    email: localStorage.getItem("email"),
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {}, [user]);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <div className="containter-fluid">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/mycart" element={<MyCart />} />
            <Route path="/orders/history" element={<OrderHistory />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />

            {/* Admin Routes */}
            <Route path="/admin/products" element={<Product />} />
            <Route path="/products/:productId" element={<ProductDetails />} />
            <Route path="/admin/users" element={<Users />} />
            <Route
              path="/admin/orders/history"
              element={<AdminOrderHistory />}
            />
            <Route
              path="/admin/products/update/:productId"
              element={<UpdateProduct />}
            />

            <Route path="/admin/products/create" element={<CreateProduct />} />

            {/* <Route
              path="/admin/products/:productId"
              element={<ArchieveProduct />}
            /> */}
            {/* <Route path="/admin/test" element={<Test />} /> */}

            <Route path="/restricted" element={<Restricted />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
      </Router>
    </UserProvider>
  );
}

export default App;
